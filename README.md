# React Study

## 시작 - 2017. 6. 15. - 15 층

- 참여 - 최용호, 김환수, 이호경, 권혜진, 진입명
- 장소 - 매주 화요일 1시 (장소 15 층)
- 방법 - 매주 주제와 관련있는 과제

- 목적
  - 포커스 - 최용호
  - 현업 프로젝트 적용
  - 목적에 맞게 프로젝트 설계
  - 최적화 된 코드, Clean Code (리뷰)
- 앞으로 배울 기술
    - Babel, Module(Import), ESLint (Slack/#articles)
    - ESLint 설정, Webpack 2, CSS 모듈링 (PostCSS, SMACSS)
    - HMR(Webpack), React Hot Loader(React only)
    - React (소스 리뷰)
    - Test (Mocha)
    - HOC, Redux
