const path = require('path')
const srcPath = path.resolve(__dirname, 'src')
const pubPath = path.resolve(__dirname, 'public')

const config = {
  /** @see {@link https://webpack.js.org/configuration/target/} */
  target: 'web',

  /** @see {@link https://webpack.js.org/configuration/devtool/} */
  devtool: 'eval',

  /** @see {@link https://webpack.js.org/configuration/entry-context} */
  context: srcPath,
  entry: [
    './index'
  ],

  /** @see {@link https://webpack.js.org/configuration/output/} */
  output: {
    path: pubPath,
    filename: 'bundle.js',
    publicPath: '/'
  },

  /** @see {@link https://webpack.js.org/configuration/resolve/} */
  resolve: {
    extensions: ['.js', '.jsx']
  },

  /** @see {@link https://webpack.js.org/configuration/dev-server/} */
  devServer: {
    contentBase: pubPath,
    host: '0.0.0.0',
    port: 4000
  }
}

module.exports = config
